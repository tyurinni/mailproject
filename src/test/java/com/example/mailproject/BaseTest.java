package com.example.mailproject;

import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.DepartureTypes;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.model.PostalDelivery;
import com.example.mailproject.service.PostOfficeService;
import com.example.mailproject.service.PostalDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BaseTest {

    protected final PostOfficeService serviceOffice;
    protected final PostalDeliveryService serviceDelivery;
    public BaseTest(PostOfficeService serviceOffice,
                    PostalDeliveryService serviceDelivery) {
        this.serviceOffice = serviceOffice;
        this.serviceDelivery = serviceDelivery;
        addPostOffice();
    }
    private void addPostOffice() {
        {
            PostOffice office = new PostOffice();
            office.setIndex("100");
            office.setTitle("Title 100");
            serviceOffice.save(office);

            PostalDelivery delivery = new PostalDelivery();
            delivery.setDeliveryOffice(office);
            delivery.setStatus(DepartureStatus.ACCEPTED);
            delivery.setDeliveryAddress("Address");
            delivery.setType(DepartureTypes.PACKAGE);
            serviceDelivery.save(delivery);
        }
        {
            PostOffice office = new PostOffice();
            office.setIndex("200");
            office.setTitle("Title 200");
            serviceOffice.save(office);
        }
        {
            PostOffice office = new PostOffice();
            office.setIndex("300");
            office.setTitle("Title 300");
            serviceOffice.save(office);
        }
    }
}
