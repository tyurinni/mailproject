package com.example.mailproject.util;

import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.DepartureTypes;
import com.example.mailproject.model.PostalDelivery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class HistoryBuilderTest {
    private final HistoryBuilder builder = new HistoryBuilder();
    @Test
    public void getHistoryTest() {
        PostalDelivery delivery = new PostalDelivery();
        delivery.setType(DepartureTypes.LETTER);
        delivery.setStatus(DepartureStatus.ACCEPTED);
        delivery.setDeliveryAddress("Address");

        String history = builder.addRecordHistory(delivery, null);
        Assertions.assertNotNull(history);

        delivery.setHistory(history);
        List<String> historyList = builder.historyMessage(delivery);
        Assertions.assertNotNull(history);
        Assertions.assertEquals(1, historyList.size());
        Assertions.assertTrue(historyList.get(0).contains(DepartureStatus.ACCEPTED.getRuValue()));
    }
}
