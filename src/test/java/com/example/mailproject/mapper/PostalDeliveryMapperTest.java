package com.example.mailproject.mapper;


import com.example.mailproject.BaseTest;
import com.example.mailproject.dto.PostalDeliveryDTO;
import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.DepartureTypes;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.model.PostalDelivery;
import com.example.mailproject.service.PostOfficeService;
import com.example.mailproject.service.PostalDeliveryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
public class PostalDeliveryMapperTest extends BaseTest {
    private final PostalDeliveryMapper mapper;

    public PostalDeliveryMapperTest(@Autowired PostalDeliveryMapper mapper,
                                    @Autowired PostOfficeService officeService,
                                    @Autowired PostalDeliveryService deliveryService) {
        super(officeService, deliveryService);
        this.mapper = mapper;
    }
    @Test
    public void testMappedDtoToEntity() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();
        PostalDeliveryDTO dto = new PostalDeliveryDTO();
        dto.setType(DepartureTypes.PACKAGE);
        dto.setStatus(DepartureStatus.ACCEPTED);
        dto.setDeliveryAddress("Address");
        dto.setPostOfficeId(office.getId());
        PostalDelivery delivery = mapper.dtoToEntity(dto);
        Assertions.assertNotNull(delivery);
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), office.getId());
        Assertions.assertEquals(delivery.getStatus(), dto.getStatus());
        Assertions.assertEquals(delivery.getType(), dto.getType());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dto.getDeliveryAddress());
    }
    @Test
    public void testMappedEntityToDto() {
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostalDelivery delivery = postalDeliveryOpt.get();
        PostalDeliveryDTO dto = mapper.entityToDto(delivery);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), dto.getPostOfficeId());
        Assertions.assertEquals(delivery.getStatus(), dto.getStatus());
        Assertions.assertEquals(delivery.getType(), dto.getType());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dto.getDeliveryAddress());
    }
}
