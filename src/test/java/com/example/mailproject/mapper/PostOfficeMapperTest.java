package com.example.mailproject.mapper;

import com.example.mailproject.BaseTest;
import com.example.mailproject.dto.PostOfficeDTO;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.service.PostOfficeService;
import com.example.mailproject.service.PostalDeliveryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
public class PostOfficeMapperTest extends BaseTest {
    private final PostOfficeMapper mapper;

    public PostOfficeMapperTest(@Autowired PostOfficeService serviceOffice,
                                @Autowired PostalDeliveryService serviceDelivery,
                                @Autowired PostOfficeMapper mapper) {
        super(serviceOffice, serviceDelivery);
        this.mapper = mapper;
    }
    @Test
    public void testMappedDtoToEntity() {
        PostOfficeDTO dto = new PostOfficeDTO();
        dto.setId(1L);
        dto.setIndex("Index");
        dto.setTitle("Title");

        PostOffice office = mapper.dtoToEntity(dto);

        Assertions.assertNotNull(office);
        Assertions.assertEquals(1L, office.getId());
        Assertions.assertEquals(dto.getIndex(), office.getIndex());
        Assertions.assertEquals(dto.getTitle(), office.getTitle());
    }
    @Test
    public void testMappedEntityToDto() {
        PostOffice office = new PostOffice();
        office.setId(1L);
        office.setTitle("Title");
        office.setIndex("Index");

        PostOfficeDTO dto = mapper.entityToDto(office);

        Assertions.assertNotNull(dto);
        Assertions.assertEquals(dto.getId(), office.getId());
        Assertions.assertEquals(dto.getTitle(), office.getTitle());
        Assertions.assertEquals(dto.getIndex(), office.getIndex());
    }
}
