package com.example.mailproject.service;

import com.example.mailproject.BaseTest;
import com.example.mailproject.dto.HistoryPostalDeliveryDTO;
import com.example.mailproject.dto.PostalDeliveryDTO;
import com.example.mailproject.dto.WorkingMessage;
import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.DepartureTypes;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.model.PostalDelivery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class MainServiceTest extends BaseTest {
    private final MainService service;

    public MainServiceTest(@Autowired MainService service,
                           @Autowired PostOfficeService officeService,
                           @Autowired PostalDeliveryService postalDeliveryService) {
        super(officeService, postalDeliveryService);
        this.service = service;
    }
    @Test
    public void testRegistration() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();

        PostalDeliveryDTO dto = new PostalDeliveryDTO();
        dto.setType(DepartureTypes.LETTER);
        dto.setDeliveryAddress("Address");
        dto.setPostOfficeId(office.getId());

        PostalDeliveryDTO result = service.registration(dto);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(dto.getType(), result.getType());
        Assertions.assertEquals(DepartureStatus.ACCEPTED, result.getStatus());
        Assertions.assertNotNull(dto.getDeliveryAddress(), result.getDeliveryAddress());
    }
    @Test
    public void testArrival() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();
        PostalDelivery delivery = postalDeliveryOpt.get();

        WorkingMessage message = new WorkingMessage();
        message.setPostOfficeId(office.getId());
        message.setPostalDeliveryId(delivery.getId());

        PostalDeliveryDTO dto = service.arrivalAtTheIntermediatePostOffice(message);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(DepartureStatus.ARRIVED, dto.getStatus());
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), dto.getPostOfficeId());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dto.getDeliveryAddress());
        Assertions.assertEquals(delivery.getType(), dto.getType());
    }
    @Test
    public void testDeparture() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();
        PostalDelivery delivery = postalDeliveryOpt.get();

        WorkingMessage message = new WorkingMessage();
        message.setPostOfficeId(office.getId());
        message.setPostalDeliveryId(delivery.getId());

        PostalDeliveryDTO dto = service.departureFromThePostOffice(message);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(DepartureStatus.DEPARTURE, dto.getStatus());
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), dto.getPostOfficeId());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dto.getDeliveryAddress());
        Assertions.assertEquals(delivery.getType(), dto.getType());
    }
    @Test
    public void testReceiving() {
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostalDelivery delivery = postalDeliveryOpt.get();

        WorkingMessage message = new WorkingMessage();
        message.setPostalDeliveryId(delivery.getId());

        PostalDeliveryDTO dto = service.receivingByTheAddressee(message);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(DepartureStatus.FINALLY_ARRIVED, dto.getStatus());
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), dto.getPostOfficeId());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dto.getDeliveryAddress());
        Assertions.assertEquals(delivery.getType(), dto.getType());
    }
    @Test
    public void testHistory() {
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostalDelivery delivery = postalDeliveryOpt.get();

        PostalDeliveryDTO dto = serviceDelivery.getDto(delivery);
        Assertions.assertNotNull(dto);

        PostalDeliveryDTO resultDel = service.registration(dto);
        Assertions.assertNotNull(resultDel);

        HistoryPostalDeliveryDTO result = service.getHistoryPostalDelivery(resultDel.getId());
        List<String> resultHistory = result.getHistory();
        Assertions.assertNotNull(resultHistory);
        Assertions.assertEquals(resultHistory.size(), 1);
    }
}
