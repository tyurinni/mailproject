package com.example.mailproject.controller;

import com.example.mailproject.BaseTest;
import com.example.mailproject.controler.MailController;
import com.example.mailproject.dto.HistoryPostalDeliveryDTO;
import com.example.mailproject.dto.PostalDeliveryDTO;
import com.example.mailproject.dto.WorkingMessage;
import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.DepartureTypes;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.model.PostalDelivery;
import com.example.mailproject.service.PostOfficeService;
import com.example.mailproject.service.PostalDeliveryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;


@SpringBootTest
public class MainControllerTest extends BaseTest {
    private final MailController controller;

    public MainControllerTest(@Autowired PostOfficeService serviceOffice,
                              @Autowired PostalDeliveryService serviceDelivery,
                              @Autowired MailController controller) {
        super(serviceOffice, serviceDelivery);
        this.controller = controller;
    }

    @Test
    public void testRegistrationController() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();

        PostalDeliveryDTO dto = new PostalDeliveryDTO();
        dto.setType(DepartureTypes.LETTER);
        dto.setDeliveryAddress("Address");
        dto.setPostOfficeId(office.getId());
        
        ResponseEntity<PostalDeliveryDTO> result = controller.registration(dto);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(result.getBody());
        PostalDeliveryDTO dtoResult = result.getBody();
        Assertions.assertEquals(dto.getType(), dtoResult.getType());
        Assertions.assertEquals(DepartureStatus.ACCEPTED, dtoResult.getStatus());
        Assertions.assertNotNull(dto.getDeliveryAddress(), dtoResult.getDeliveryAddress());
    }
    @Test
    public void testArrivalController() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();
        PostalDelivery delivery = postalDeliveryOpt.get();

        WorkingMessage message = new WorkingMessage();
        message.setPostOfficeId(office.getId());
        message.setPostalDeliveryId(delivery.getId());

        ResponseEntity<PostalDeliveryDTO> result = controller.arrival(message);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(result.getBody());
        PostalDeliveryDTO dtoResult = result.getBody();
        Assertions.assertNotNull(dtoResult);
        Assertions.assertEquals(DepartureStatus.ARRIVED, dtoResult.getStatus());
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), dtoResult.getPostOfficeId());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dtoResult.getDeliveryAddress());
        Assertions.assertEquals(delivery.getType(), dtoResult.getType());
    }
    @Test
    public void testDepartureController() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();
        PostalDelivery delivery = postalDeliveryOpt.get();

        WorkingMessage message = new WorkingMessage();
        message.setPostOfficeId(office.getId());
        message.setPostalDeliveryId(delivery.getId());

        ResponseEntity<PostalDeliveryDTO> result = controller.departure(message);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(result.getBody());
        PostalDeliveryDTO dtoResult = result.getBody();
        Assertions.assertNotNull(dtoResult);
        Assertions.assertEquals(DepartureStatus.DEPARTURE, dtoResult.getStatus());
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), dtoResult.getPostOfficeId());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dtoResult.getDeliveryAddress());
        Assertions.assertEquals(delivery.getType(), dtoResult.getType());
    }
    @Test
    public void testReceivingController() {
        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());
        PostalDelivery delivery = postalDeliveryOpt.get();

        WorkingMessage message = new WorkingMessage();
        message.setPostalDeliveryId(delivery.getId());

        ResponseEntity<PostalDeliveryDTO> result = controller.receiving(message);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(result.getBody());

        PostalDeliveryDTO dto = result.getBody();
        Assertions.assertEquals(DepartureStatus.FINALLY_ARRIVED, dto.getStatus());
        Assertions.assertEquals(delivery.getDeliveryOffice().getId(), dto.getPostOfficeId());
        Assertions.assertEquals(delivery.getDeliveryAddress(), dto.getDeliveryAddress());
        Assertions.assertEquals(delivery.getType(), dto.getType());
    }
    @Test
    public void testHistoryController() {
        Optional<PostOffice> postOfficeOpt = serviceOffice.getAllEntity().stream().findFirst();
        Assertions.assertFalse(postOfficeOpt.isEmpty());
        PostOffice office = postOfficeOpt.get();

        PostalDeliveryDTO dto = new PostalDeliveryDTO();
        dto.setStatus(DepartureStatus.ACCEPTED);
        dto.setType(DepartureTypes.LETTER);
        dto.setDeliveryAddress("Address test");
        dto.setPostOfficeId(office.getId());
        controller.registration(dto);

        Optional<PostalDelivery> postalDeliveryOpt = serviceDelivery.getAllEntity()
                .stream().filter(p -> p.getDeliveryAddress().equals("Address test")).findFirst();
        Assertions.assertFalse(postalDeliveryOpt.isEmpty());

        PostalDelivery delivery = postalDeliveryOpt.get();

        ResponseEntity<HistoryPostalDeliveryDTO> result = controller.history(delivery.getId());
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(result.getBody());

        HistoryPostalDeliveryDTO historyDto = result.getBody();
        List<String> first = List.of(delivery.getHistory().split(";"));
        List<String> second = historyDto.getHistory();
        Assertions.assertEquals(first.size(), second.size());
        for (int i = 0; i < first.size(); i++) {
            Assertions.assertEquals(first.get(i), second.get(i));
        }
    }
}
