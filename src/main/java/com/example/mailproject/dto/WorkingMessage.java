package com.example.mailproject.dto;

import com.example.mailproject.model.DepartureStatus;
import lombok.Data;

@Data
public class WorkingMessage {
    private Long postalDeliveryId;
    private Long postOfficeId;
}
