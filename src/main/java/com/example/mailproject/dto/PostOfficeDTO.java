package com.example.mailproject.dto;

import lombok.Data;

@Data
public class PostOfficeDTO extends BaseDTO{
    private String index;
    private String title;
}
