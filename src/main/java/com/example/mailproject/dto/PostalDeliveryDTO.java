package com.example.mailproject.dto;

import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.DepartureTypes;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class PostalDeliveryDTO extends BaseDTO{
    private DepartureTypes type;
    private DepartureStatus status;
    private Long postOfficeId;
    private String deliveryAddress;
}
