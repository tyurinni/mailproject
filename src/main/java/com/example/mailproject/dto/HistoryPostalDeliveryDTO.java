package com.example.mailproject.dto;

import lombok.Data;

import java.util.List;

@Data
public class HistoryPostalDeliveryDTO {
    private List<String> history;
}
