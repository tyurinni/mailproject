package com.example.mailproject.dto;

import lombok.Data;

@Data
public abstract class BaseDTO {
    private Long id;
}
