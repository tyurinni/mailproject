package com.example.mailproject.mapper;

import com.example.mailproject.dto.PostalDeliveryDTO;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.model.PostalDelivery;
import com.example.mailproject.repository.PostOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Function;

@Component
public class PostalDeliveryMapper implements Mapper<PostalDelivery, PostalDeliveryDTO> {
    private final PostOfficeRepository officeRepository;

    public PostalDeliveryMapper(@Autowired PostOfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    @Override
    public Function<PostalDelivery, PostalDeliveryDTO> configurationForEntityToDTO() {
        return (postalDelivery) -> {
          PostalDeliveryDTO dto = new PostalDeliveryDTO();
          dto.setId(postalDelivery.getId());
          dto.setStatus(postalDelivery.getStatus());
          dto.setPostOfficeId(postalDelivery.getDeliveryOffice().getId());
          dto.setDeliveryAddress(postalDelivery.getDeliveryAddress());
          dto.setType(postalDelivery.getType());
          return dto;
        };
    }

    @Override
    public Function<PostalDeliveryDTO, PostalDelivery> configurationForDTOtoEntity() {
        return (dto) -> {
            PostalDelivery delivery = new PostalDelivery();
            if (Objects.nonNull(dto.getId())) {
                delivery.setId(dto.getId());
            }
            if (Objects.nonNull(dto.getDeliveryAddress())) {
                String address = dto.getDeliveryAddress().trim();
                if (address.isEmpty()) {
                    throw new IllegalArgumentException(address);
                }
                delivery.setDeliveryAddress(address);
            }
            if (Objects.nonNull(dto.getType())) {
                delivery.setType(dto.getType());
            } else {
                throw new NullPointerException();
            }
            if (Objects.nonNull(dto.getStatus())) {
                delivery.setStatus(dto.getStatus());
            }
            if (Objects.nonNull(dto.getPostOfficeId())) {
                PostOffice office = officeRepository.findById(dto.getPostOfficeId()).orElseThrow(IllegalArgumentException::new);
                delivery.setDeliveryOffice(office);
            } else {
                throw new NullPointerException();
            }
            return delivery;
        };
    }
}
