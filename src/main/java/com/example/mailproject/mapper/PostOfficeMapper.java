package com.example.mailproject.mapper;

import com.example.mailproject.dto.PostOfficeDTO;
import com.example.mailproject.model.PostOffice;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

@Component
public class PostOfficeMapper implements Mapper<PostOffice, PostOfficeDTO> {
    @Override
    public Function<PostOffice, PostOfficeDTO> configurationForEntityToDTO() {
        return postOffice -> {
            PostOfficeDTO dto = new PostOfficeDTO();
            dto.setId(postOffice.getId());
            dto.setTitle(postOffice.getTitle());
            dto.setIndex(postOffice.getIndex());
            return dto;
        };
    }

    @Override
    public Function<PostOfficeDTO, PostOffice> configurationForDTOtoEntity() {
        return officeDTO -> {
          PostOffice office = new PostOffice();
            if (Objects.nonNull(officeDTO.getIndex())) {
                String index = officeDTO.getIndex().trim();
                if (index.isEmpty()) {
                    throw new IllegalArgumentException(index);
                }
                office.setIndex(index);
            }
            if (Objects.nonNull(officeDTO.getTitle())) {
                String title = officeDTO.getTitle().trim();
                if (title.isEmpty()) {
                    throw new IllegalArgumentException(title);
                }
                office.setTitle(title);
            }
            if (Objects.nonNull(officeDTO.getId())) {
                office.setId(officeDTO.getId());
            }
            return office;
        };
    }
}
