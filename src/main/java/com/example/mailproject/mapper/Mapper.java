package com.example.mailproject.mapper;

import com.example.mailproject.dto.BaseDTO;
import com.example.mailproject.model.BaseEntity;

import java.util.function.Function;

public interface Mapper<E extends BaseEntity, D extends BaseDTO> {
    default E dtoToEntity(D dto) {
        Function<D, E> function = configurationForDTOtoEntity();
        return function.apply(dto);
    }

    default D entityToDto(E entity) {
        Function<E, D> function = configurationForEntityToDTO();
        return function.apply(entity);
    }
    Function<E, D> configurationForEntityToDTO();
    Function<D, E> configurationForDTOtoEntity();
}
