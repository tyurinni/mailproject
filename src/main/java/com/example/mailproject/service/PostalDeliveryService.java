package com.example.mailproject.service;

import com.example.mailproject.dto.PostalDeliveryDTO;
import com.example.mailproject.mapper.PostalDeliveryMapper;
import com.example.mailproject.model.PostalDelivery;
import com.example.mailproject.repository.PostalDeliveryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostalDeliveryService extends BaseService<PostalDelivery, PostalDeliveryDTO> {
    public PostalDeliveryService(@Autowired PostalDeliveryRepository repository,
                                 @Autowired PostalDeliveryMapper mapper) {
        super(repository, mapper);
    }
}
