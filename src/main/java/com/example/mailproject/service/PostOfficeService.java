package com.example.mailproject.service;

import com.example.mailproject.dto.PostOfficeDTO;
import com.example.mailproject.mapper.PostOfficeMapper;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.repository.PostOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostOfficeService extends BaseService<PostOffice, PostOfficeDTO> {
    public PostOfficeService(@Autowired PostOfficeRepository repository,
                             @Autowired PostOfficeMapper mapper) {
        super(repository, mapper);
    }
}
