package com.example.mailproject.service;

import com.example.mailproject.dto.BaseDTO;
import com.example.mailproject.mapper.Mapper;
import com.example.mailproject.model.BaseEntity;
import com.example.mailproject.repository.BaseRepository;

import java.util.List;

public abstract class BaseService<E extends BaseEntity, D extends BaseDTO> {
    final BaseRepository<E> repository;
    final Mapper<E, D> mapper;
    BaseService(BaseRepository<E> repository, Mapper<E, D> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }
    public E getEntity(D dto){
        return mapper.dtoToEntity(dto);
    }
    public D getDto(E entity){
        return mapper.entityToDto(entity);
    }

    public E save(E entity){
        return repository.save(entity);
    }

    public void delete(E entity){
        repository.delete(entity);
    }
    public List<E> getAllEntity() {
        return repository.findAll();
    }

    public E findById(Long id){
        return repository.findById(id).orElseThrow(NullPointerException::new);
    }
}
