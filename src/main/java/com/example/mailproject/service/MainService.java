package com.example.mailproject.service;

import com.example.mailproject.dto.WorkingMessage;
import com.example.mailproject.dto.HistoryPostalDeliveryDTO;
import com.example.mailproject.dto.PostalDeliveryDTO;
import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.model.PostalDelivery;
import com.example.mailproject.util.HistoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MainService {
    private final PostalDeliveryService postalDeliveryService;
    private final PostOfficeService postOfficeService;
    private final HistoryBuilder historyBuilder;

    public MainService(@Autowired PostalDeliveryService postalDeliveryService,
                       @Autowired PostOfficeService postOfficeService,
                       @Autowired HistoryBuilder historyBuilder) {
        this.postalDeliveryService = postalDeliveryService;
        this.postOfficeService = postOfficeService;
        this.historyBuilder = historyBuilder;
    }

    public PostalDeliveryDTO registration(final PostalDeliveryDTO deliveryDTO) {
        PostalDelivery delivery = postalDeliveryService.getEntity(deliveryDTO);
        delivery.setStatus(DepartureStatus.ACCEPTED);
        delivery.setHistory(historyBuilder.addRecordHistory(delivery, null));
        postalDeliveryService.save(delivery);
        return postalDeliveryService.getDto(delivery);
    }

    public PostalDeliveryDTO arrivalAtTheIntermediatePostOffice(final WorkingMessage message) {
        PostalDelivery delivery = postalDeliveryService.findById(message.getPostalDeliveryId());
        PostOffice office = postOfficeService.findById(message.getPostOfficeId());
        delivery.setStatus(DepartureStatus.ARRIVED);
        delivery.setHistory(historyBuilder.addRecordHistory(delivery, office));
        postalDeliveryService.save(delivery);
        return postalDeliveryService.getDto(delivery);
    }

    public PostalDeliveryDTO departureFromThePostOffice(final WorkingMessage message) {
        PostalDelivery delivery = postalDeliveryService.findById(message.getPostalDeliveryId());
        PostOffice office = postOfficeService.findById(message.getPostOfficeId());
        delivery.setStatus(DepartureStatus.DEPARTURE);
        delivery.setHistory(historyBuilder.addRecordHistory(delivery, office));
        postalDeliveryService.save(delivery);
        return postalDeliveryService.getDto(delivery);
    }

    public PostalDeliveryDTO receivingByTheAddressee(final WorkingMessage message) {
        PostalDelivery delivery = postalDeliveryService.findById(message.getPostalDeliveryId());
        delivery.setStatus(DepartureStatus.FINALLY_ARRIVED);
        delivery.setHistory(historyBuilder.addRecordHistory(delivery, delivery.getDeliveryOffice()));
        postalDeliveryService.save(delivery);
        return postalDeliveryService.getDto(delivery);
    }

    public HistoryPostalDeliveryDTO getHistoryPostalDelivery(final Long id) {
        PostalDelivery delivery = postalDeliveryService.findById(id);
        HistoryPostalDeliveryDTO historyDTO = new HistoryPostalDeliveryDTO();
        historyDTO.setHistory(historyBuilder.historyMessage(delivery));
        return historyDTO;
    }
}
