package com.example.mailproject.controler;

import com.example.mailproject.dto.WorkingMessage;
import com.example.mailproject.dto.HistoryPostalDeliveryDTO;
import com.example.mailproject.dto.PostOfficeDTO;
import com.example.mailproject.dto.PostalDeliveryDTO;
import com.example.mailproject.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/app")
public class MailController {
    private final MainService service;

    public MailController(@Autowired MainService service) {
        this.service = service;
    }
    @RequestMapping(method = RequestMethod.POST, value = "/registration")
    public ResponseEntity<PostalDeliveryDTO> registration(@RequestBody PostalDeliveryDTO deliveryDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(service.registration(deliveryDTO));
    }
    @RequestMapping(method = RequestMethod.PUT, value = "/arrival")
    public ResponseEntity<PostalDeliveryDTO> arrival(@RequestBody WorkingMessage message) {
        return ResponseEntity.status(HttpStatus.OK).body(service.arrivalAtTheIntermediatePostOffice(message));
    }
    @RequestMapping(method = RequestMethod.PUT, value = "/departure")
    public ResponseEntity<PostalDeliveryDTO> departure(@RequestBody WorkingMessage message) {
        return ResponseEntity.status(HttpStatus.OK).body(service.departureFromThePostOffice(message));
    }
    @RequestMapping(method = RequestMethod.PUT, value = "/receiving")
    public ResponseEntity<PostalDeliveryDTO> receiving(@RequestBody WorkingMessage message) {
        return ResponseEntity.status(HttpStatus.OK).body(service.receivingByTheAddressee(message));
    }
    @RequestMapping(method = RequestMethod.GET, value = "/history/{id}")
    public ResponseEntity<HistoryPostalDeliveryDTO> history(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(service.getHistoryPostalDelivery(id));
    }
}
