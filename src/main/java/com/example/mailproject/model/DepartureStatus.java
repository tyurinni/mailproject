package com.example.mailproject.model;

import lombok.Getter;

@Getter
public enum DepartureStatus {
    FINALLY_ARRIVED("Прибыло конечному адресату"),
    ARRIVED("Прибыло"),
    DEPARTURE("Отправлено"),
    ACCEPTED("Принято в отделении");
    DepartureStatus(final String ruValue){
        this.ruValue = ruValue;
    }
    private final String ruValue;
}
