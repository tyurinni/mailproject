package com.example.mailproject.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Entity
@Table(name = "post_office")
@Data
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "default_generator", allocationSize = 1, sequenceName = "post_office_seq")
public class PostOffice extends BaseEntity{
    @Column(name = "index")
    private String index;
    @Column(name = "title")
    private String title;
}
