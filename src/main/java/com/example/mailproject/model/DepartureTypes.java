package com.example.mailproject.model;

import lombok.Getter;

@Getter
public enum DepartureTypes {
    LETTER("Письмо"),
    PACKAGE("Посылка"),
    PARCEL("Бандероль"),
    POSTCARD("Открытка");
    DepartureTypes(final String ruValue) {
        this.ruValue = ruValue;
    }
    private final String ruValue;
}
