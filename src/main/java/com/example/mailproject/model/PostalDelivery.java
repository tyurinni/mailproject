package com.example.mailproject.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "postal_delivery")
@Data
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "default_generator", allocationSize = 1, sequenceName = "postal_delivery_seq")
public class PostalDelivery extends BaseEntity{
    @Enumerated
    @Column(name = "type")
    private DepartureTypes type;
    @Enumerated
    @Column(name = "status")
    private DepartureStatus status;
    @ManyToOne
    private PostOffice deliveryOffice;
    @Column(name = "address")
    private String deliveryAddress;
    @Column(name = "history", length = 2000)
    private String history;
}
