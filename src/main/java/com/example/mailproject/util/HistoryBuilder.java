package com.example.mailproject.util;

import com.example.mailproject.model.DepartureStatus;
import com.example.mailproject.model.PostOffice;
import com.example.mailproject.model.PostalDelivery;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class HistoryBuilder {
    public String addRecordHistory(final PostalDelivery delivery, final PostOffice postOffice) {
        String record = buildRecord(delivery, postOffice);
        List<String> historyArray = getHistoryArray(delivery.getHistory());
        historyArray.add(record);
        return getHistory(historyArray);
    }

    public List<String> historyMessage(final PostalDelivery delivery) {
        return getHistoryArray(delivery.getHistory());
    }
    private List<String> getHistoryArray(final String history) {
        if (Objects.isNull(history)) {
            return new ArrayList<>();
        }
        return Arrays.stream(history.trim().split(";")).collect(Collectors.toCollection(ArrayList::new));
    }
    private String getHistory(List<String> historyArray) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < historyArray.size(); i++) {
            if (i == (historyArray.size() - 1)) {
                builder.append(historyArray.get(i));
            } else {
                builder.append(historyArray.get(i)).append(";");
            }
        }
        return builder.toString();
    }
    private String buildRecord(final PostalDelivery delivery, final PostOffice postOffice) {
        StringBuilder builder = new StringBuilder();
        builder.append("(").append(delivery.getStatus().getRuValue()).append(",");
        if (delivery.getStatus().equals(DepartureStatus.ACCEPTED)) {
            builder.append(delivery.getType().getRuValue()).append(",");
        }
        if (Objects.isNull(postOffice)) {
            builder.append("Post Office");
        } else {
            builder.append(postOffice.getIndex());
        }
        builder.append(",").append(LocalDateTime.now());
        builder.append(")");
        return builder.toString();
    }
}
