package com.example.mailproject.repository;

import com.example.mailproject.model.PostOffice;
import org.springframework.stereotype.Repository;

@Repository
public interface PostOfficeRepository extends BaseRepository<PostOffice>{
}
