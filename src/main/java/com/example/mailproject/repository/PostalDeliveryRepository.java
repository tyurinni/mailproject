package com.example.mailproject.repository;

import com.example.mailproject.model.PostalDelivery;
import org.springframework.stereotype.Repository;

@Repository
public interface PostalDeliveryRepository extends BaseRepository<PostalDelivery>{
}
